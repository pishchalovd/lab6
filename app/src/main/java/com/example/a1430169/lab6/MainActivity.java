package com.example.a1430169.lab6;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        makeNewUI("lab6", 1);
    }

    private void makeNewUI(String tag, int ix) {
        // before we call this code we have a handle on the table:
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainlayout);
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        TextView txt1 = new TextView(this);
        txt1.setText("Loan Calculator");
        txt1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        txt1.setTextSize(16);
        txt1.setGravity(View.TEXT_ALIGNMENT_CENTER);
        mainLayout.addView(txt1);

    LinearLayout string1 =new LinearLayout(this);
        string1.setOrientation(LinearLayout.HORIZONTAL);
        string1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string1);

        TextView txt11 = new TextView(this);
        txt11.setText("Loan Amount");
        txt11.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
        txt11.setTextSize(16);
        string1.addView(txt11);

        EditText et11 = new EditText(this);
        et11.setId(R.id.amountField);
        et11.setText("");
        et11.setLayoutParams(new LinearLayout.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        string1.addView(et11);

        LinearLayout string2 =new LinearLayout(this);
        string2.setOrientation(LinearLayout.HORIZONTAL);
        string2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string2);

        TextView txt12 = new TextView(this);
        txt12.setText("Term of loan (years)");
        txt12.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
        txt12.setTextSize(16);
        string2.addView(txt12);

        EditText et12 = new EditText(this);
        et12.setId(R.id.termField);
        et12.setText("");
        et12.setLayoutParams(new LinearLayout.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        string2.addView(et12);

        LinearLayout string3 =new LinearLayout(this);
        string3.setOrientation(LinearLayout.HORIZONTAL);
        string3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string3);

        TextView txt13 = new TextView(this);
        txt13.setText("Loan Amount");
        txt13.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
        txt13.setTextSize(16);
        string3.addView(txt13);

        EditText et13 = new EditText(this);
        et13.setId(R.id.rateField);
        et13.setText("");
        et13.setLayoutParams(new LinearLayout.LayoutParams(0,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        string3.addView(et13);

        LinearLayout string4 =new LinearLayout(this);
        string4.setOrientation(LinearLayout.HORIZONTAL);
        string4.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string4);

        Button btnCalculate = new Button(this);
        btnCalculate.setText("CALCULATE");
        btnCalculate.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
        btnCalculate.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showData(v);
                                            }
                                        });
        btnCalculate.setGravity(View.TEXT_ALIGNMENT_CENTER);
        string4.addView(btnCalculate);

        Button btnClear = new Button(this);
        btnClear.setText("CLEAR");
        btnClear.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData(v);
            }
        });
        btnClear.setGravity(View.TEXT_ALIGNMENT_CENTER);
        string4.addView(btnClear);

        LinearLayout string5 =new LinearLayout(this);
        string5.setOrientation(LinearLayout.HORIZONTAL);
        string5.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string5);

        TextView txt15 = new TextView(this);
        txt15.setText("Monthly payment");
        txt15.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt15.setTextSize(16);

        string5.addView(txt15);

        TextView txt25 = new TextView(this);
        txt25.setText("");
        txt25.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt25.setTextSize(16);
        txt25.setId(R.id.monthlyValue);
        string5.addView(txt25);

        LinearLayout string6 =new LinearLayout(this);
        string6.setOrientation(LinearLayout.HORIZONTAL);
        string6.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string6);

        TextView txt16 = new TextView(this);
        txt16.setText("Total payment");
        txt16.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt16.setTextSize(16);
        string6.addView(txt16);

        TextView txt26 = new TextView(this);
        txt26.setText("");
        txt26.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt26.setTextSize(16);
        txt26.setId(R.id.totalValue);
        string6.addView(txt26);

        LinearLayout string7 =new LinearLayout(this);
        string7.setOrientation(LinearLayout.HORIZONTAL);
        string7.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        mainLayout.addView(string7);

        TextView txt17 = new TextView(this);
        txt17.setText("Total interest");
        txt17.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt17.setTextSize(16);
        string7.addView(txt17);

        TextView txt27 = new TextView(this);
        txt27.setText("");
        txt27.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        txt27.setTextSize(16);
        txt27.setId(R.id.intValue);
        string7.addView(txt27);





    } //makeNewUI()

    public void showData(View view) {
        // get the references to the view widgets
        // no reference to the header TextView, I don't manipulate it
        EditText et = (EditText) findViewById(R.id.amountField);
        double amount = Double.parseDouble(et.getText().toString());

        et = (EditText) findViewById(R.id.termField);
        int term = Integer.parseInt(et.getText().toString());

        et = (EditText) findViewById(R.id.rateField);
        double rate = Double.parseDouble(et.getText().toString());
        LoanCalculator lc = new LoanCalculator(amount, term, rate);

        TextView tv1 = (TextView)findViewById(R.id.monthlyValue);
        String str1 = Double.toString(lc.getMonthlyPayment());
        tv1.setText(str1);

        TextView tv2 = (TextView)findViewById(R.id.totalValue);
        String str2 = Double.toString(lc.getTotalCostOfLoan());
        tv2.setText(str2);

        TextView tv3 = (TextView)findViewById(R.id.intValue);
        String str3 = Double.toString(lc.getTotalInterest());
        tv3.setText(str3);


    }

    public void clearData(View view) {
        ((EditText)findViewById(R.id.amountField)).setText("");
        ((EditText)findViewById(R.id.termField)).setText("");
        ((EditText)findViewById(R.id.rateField)).setText("");




    }

}
